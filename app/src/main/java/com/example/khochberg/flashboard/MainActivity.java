package com.example.khochberg.flashboard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    private Context context;

    private TabHost tabHost;
    private Spinner spinner;
    private Spinner spinner2;
    private CheckBox checkBox;
    private Button start;
    private Button insert;
    private EditText questionIn;
    private EditText answerIn;

    private ArrayAdapter<String> adapter;
    protected static FBdbAdapter dbAdapt;
    private QuestionItem question;

    static final int NEW_SUBJECT_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbAdapt = new FBdbAdapter(this);
        dbAdapt.open();
        context = getApplicationContext();

        ArrayList<String> subjectList = new ArrayList<String>();
        subjectList.add("Choose a subject...");

        String fontPath = "fonts/Anjelika Rose.ttf";

        Typeface tf = Typeface.createFromAsset(MainActivity.this.getResources().getAssets(), fontPath);

        tabHost = (TabHost) findViewById(R.id.tabhost);

        tabHost.setup();

        TabHost.TabSpec tabSpec= tabHost.newTabSpec("review");
        tabSpec.setContent(R.id.tabReview).setIndicator("Review");
        tabHost.addTab(tabSpec);

        tabSpec= tabHost.newTabSpec("cards");
        tabSpec.setContent(R.id.tabCards).setIndicator("Insert Cards");
        tabHost.addTab(tabSpec);

        for(int i=0; i < tabHost.getTabWidget().getChildCount(); i++) {
            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(Color.WHITE);
            tv.setTypeface(tf);
            tv.setTextSize(25);
        }

        spinner = (Spinner) findViewById(R.id.spinner);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        adapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, subjectList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Cursor c = dbAdapt.getSubjects();
        if (c.getCount() != 0) {
            c.moveToFirst();
            int count = 0;
            while (count < c.getCount()) {
                adapter.add(c.getString(1));
                c.moveToNext();
                count++;
            }
        }
        spinner.setAdapter(adapter);
        spinner2.setAdapter(adapter);

        checkBox = (CheckBox) findViewById(R.id.checkBox);
        checkBox.setTypeface(tf);
        checkBox.setTextColor(Color.WHITE);

        start = (Button) findViewById(R.id.button);
        start.setTypeface(tf);
        start.setTextColor(Color.WHITE);

        insert = (Button) findViewById(R.id.button2);
        insert.setTypeface(tf);
        insert.setTextColor(Color.WHITE);

        questionIn = (EditText) findViewById(R.id.editText);
        answerIn = (EditText) findViewById(R.id.editText2);
        questionIn.setTypeface(tf);
        answerIn.setTypeface(tf);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startReview(View v) {
        Intent intent = new Intent(this, ReviewCards.class);
        if (spinner.getSelectedItem().toString().equals("Choose a subject...")) {
            Toast.makeText(this, "Please select a subject!", Toast.LENGTH_SHORT).show();
            return;
        }
        intent.putExtra("SUBJECT", spinner.getSelectedItem().toString());
        intent.putExtra("RANDOM", checkBox.isChecked());
        startActivity(intent);
    }

    public void addSubject(View v) {
        Intent intent = new Intent(this, NewSubject.class);
        startActivityForResult(intent, NEW_SUBJECT_REQUEST);
    }

    public void insertCard(View v) {
        if (spinner2.getSelectedItem().toString().equals("Choose a subject...")) {
            Toast.makeText(this, "Please select a subject!", Toast.LENGTH_SHORT).show();
            return;
        }
        String q = questionIn.getText().toString().trim();
        if (q.equals("")) {
            Toast.makeText(this, "Please enter a question!", Toast.LENGTH_SHORT).show();
            return;
        }
        String a = answerIn.getText().toString().trim();
        if (a.equals("")) {
            Toast.makeText(this, "Please enter an answer!", Toast.LENGTH_SHORT).show();
            return;
        }
        question = new QuestionItem(spinner2.getSelectedItem().toString(), q, a);
        dbAdapt.insertQuestion(question);
        questionIn.setText("");
        answerIn.setText("");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == NEW_SUBJECT_REQUEST) {
            if (resultCode == RESULT_OK) {
                String subject = data.getStringExtra("subject");
                dbAdapt.insertSubject(subject);
                adapter.add(subject);
                spinner.setAdapter(adapter);
                spinner2.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                spinner2.setSelection(adapter.getPosition(subject));
                spinner.setSelection(adapter.getPosition(subject));
            }
            if (resultCode == RESULT_CANCELED) {
                return;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbAdapt.close();
    }
}