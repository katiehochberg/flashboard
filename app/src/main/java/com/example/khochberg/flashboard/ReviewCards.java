package com.example.khochberg.flashboard;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;


public class ReviewCards extends ActionBarActivity {

    private GestureDetectorCompat detector;
    private String subject;
    private TextView cardView;
    private Button flip;
    private boolean random;
    private ArrayList<Integer> randArray;
    Cursor c;
    private int current;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_cards);

        detector = new GestureDetectorCompat(this, new MyGestureListener());

        String fontPath = "fonts/Anjelika Rose.ttf";

        Typeface tf = Typeface.createFromAsset(ReviewCards.this.getResources().getAssets(), fontPath);


        Intent intent = getIntent();
        subject = intent.getStringExtra("SUBJECT");
        random = intent.getBooleanExtra("RANDOM", false);

        flip = (Button) findViewById(R.id.button4);
        cardView = (TextView) findViewById(R.id.questionView);

        flip.setTextColor(Color.WHITE);
        flip.setTypeface(tf);
        cardView.setTextColor(Color.WHITE);
        cardView.setTypeface(tf);

        c = MainActivity.dbAdapt.getFlashcards(subject);
        if (c.getCount() == 0) {
            cardView.setText("There are no flashcards for this subject!");
            flip.setVisibility(View.GONE);
        } else {
            if (!random) {
                c.moveToFirst();
            } else {
                randArray();
                c.moveToPosition(randArray.get(0));
            }
            cardView.setText(c.getString(2));
            Toast.makeText(this, "Swipe left and right to change cards!", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_review_cards, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.trash) {
            if (c.getCount() == 0) {
                return true;
            }
            int qID = Integer.parseInt(c.getString(0));
            int position = c.getPosition();
            MainActivity.dbAdapt.removeQuestion(qID);
            c = MainActivity.dbAdapt.getFlashcards(subject);
            if (position < c.getCount()) {
                c.moveToPosition(position);
                cardView.setText(c.getString(2));
            } else if (c.getCount() == 0) {
                cardView.setText("There are no flashcards for this subject!");
                flip.setVisibility(View.GONE);
            } else {
                if (!random) {
                    c.moveToFirst();
                } else {
                    current--;
                    if (current < 0) {
                        current = randArray.size() - 1;
                    }
                    while (randArray.get(current) >= c.getCount()) {
                        if (current == 0) {
                            current = randArray.size() - 1;
                        } else {
                            current--;
                        }
                    }
                    c.moveToPosition(randArray.get(current));
                }
                cardView.setText(c.getString(2));
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void flipCard(View v) {
        if (c.getCount() == 0) {
            return;
        }
        if (cardView.getText().toString().equals(c.getString(2))) {
            cardView.setText(c.getString(3));
        } else {
            cardView.setText(c.getString(2));
        }
    }

    public void randArray() {
        randArray = new ArrayList<Integer>();
        boolean bool[] = new boolean[c.getCount()];
        Arrays.fill(bool, Boolean.FALSE);
        Random rand = new Random();
        int min = 0;
        int max = c.getCount() - 1;
        int count = 0;
        if (c.getCount() == 0) {
            randArray.add(0);
            return;
        }
        while (count < c.getCount()) {
            int randNum = rand.nextInt((max - min) + 1) + min;
            if (!bool[randNum]) {
                bool[randNum] = true;
                randArray.add(randNum);
                count++;
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.detector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {

            if(event1.getX() - event2.getX() > 120 && Math.abs(velocityX) > 200) {
                // Left swipe
                if (!random) {
                    if (c.isLast()) {
                        c.moveToFirst();
                    } else {
                        c.moveToNext();
                    }
                    cardView.setText(c.getString(2));
                } else {
                    current++;
                    if (current == randArray.size()) {
                        current = 0;
                    }
                    while (randArray.get(current) >= c.getCount()) {
                        if (current == (randArray.size() - 1)) {
                            current = 0;
                        } else {
                            current++;
                        }
                    }
                    c.moveToPosition(randArray.get(current));
                    cardView.setText(c.getString(2));
                }
            } else {
                if (event2.getX() - event1.getX() > 120 && Math.abs(velocityX) > 200) {
                    // Right swipe
                    if (!random) {
                        if (c.isFirst()) {
                            c.moveToLast();
                        } else {
                            c.moveToPrevious();
                        }
                        cardView.setText(c.getString(2));
                    } else {
                        current--;
                        if (current < 0) {
                            current = randArray.size() - 1;
                        }
                        while (randArray.get(current) >= c.getCount()) {
                            if (current == 0) {
                                current = randArray.size() - 1;
                            } else {
                                current--;
                            }
                        }
                        c.moveToPosition(randArray.get(current));
                        cardView.setText(c.getString(2));
                    }
                }
            }
            return true;
        }
    }
}
