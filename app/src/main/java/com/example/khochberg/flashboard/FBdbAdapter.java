package com.example.khochberg.flashboard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Kathryn on 2/27/2015.
 */
public class FBdbAdapter {

    private SQLiteDatabase db;
    private FBdbHelper dbHelper;
    private final Context context;

    private static final String DB_NAME = "fb.db";
    private static final int DB_VERSION = 1;

    private static final String SUBJECT_TABLE = "subjects";
    private static final String SUBJECT_ID = "subject_id";
    private static final String SUBJECT_NAME = "subject";
    private static final String QUESTION_TABLE = "questions";
    private static final String QUESTION_ID = "question_id";
    private static final String SUBJECT = "subject";
    private static final String QUESTION = "question";
    private static final String ANSWER = "answer";

    public FBdbAdapter(Context ctx) {
        context = ctx;
        dbHelper = new FBdbHelper(context, DB_NAME, null, DB_VERSION);
    }

    public void open() throws SQLiteException {
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException ex) {
            db = dbHelper.getReadableDatabase();
        }
    }

    public void close() {
        db.close();
    }

    public long insertSubject(String s) {
        ContentValues cvalues = new ContentValues();
        cvalues.put(SUBJECT_NAME, s);
        return db.insert(SUBJECT_TABLE, null, cvalues);
    }

    public Cursor getSubjects() {
        String selectAll = "SELECT * FROM " + SUBJECT_TABLE;
        Cursor c = db.rawQuery(selectAll ,null);
        return c;
    }

    public long insertQuestion(QuestionItem q) {
        ContentValues cvalues = new ContentValues();
        cvalues.put(SUBJECT, q.getSubject());
        cvalues.put(QUESTION, q.getQuestion());
        cvalues.put(ANSWER, q.getAnswer());
        return db.insert(QUESTION_TABLE, null, cvalues);
    }

    public boolean removeQuestion(long ri) {
        return db.delete(QUESTION_TABLE, QUESTION_ID+"="+ri, null) > 0;
    }

    public Cursor getFlashcards(String s) {
        s = s.trim();
        String subject = "SELECT * FROM questions WHERE subject = '" + s + "'";
        Cursor c = db.rawQuery(subject, null);
        return c;
    }

    private static class FBdbHelper extends SQLiteOpenHelper {

        private static final String DB_Q_CREATE = "CREATE TABLE " + QUESTION_TABLE + "("
                + QUESTION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + SUBJECT + " TEXT, "
                + QUESTION + " TEXT, " +  ANSWER + " TEXT);";

        private static final String DB_S_CREATE = "CREATE TABLE " + SUBJECT_TABLE + "("
                + SUBJECT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + SUBJECT_NAME + " TEXT);";

        public FBdbHelper(Context context, String name, SQLiteDatabase.CursorFactory fct, int version) {
            super(context, name, fct, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_Q_CREATE);
            db.execSQL(DB_S_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w("FBdb", "upgrading from version " + oldVersion + " to "
                    + newVersion + ", destroying old data");
            db.execSQL("DROP TABLE IF EXISTS " + QUESTION_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + SUBJECT_TABLE);
            onCreate(db);
        }
    }


}
