package com.example.khochberg.flashboard;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class NewSubject extends ActionBarActivity {

    private EditText subject;
    private Button cancel;
    private Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_subject);

        String fontPath = "fonts/Anjelika Rose.ttf";
        Typeface tf = Typeface.createFromAsset(NewSubject.this.getResources().getAssets(), fontPath);
        cancel = (Button) findViewById(R.id.cancelButton);
        save = (Button) findViewById(R.id.saveButton);
        subject = (EditText) findViewById(R.id.editText3);
        cancel.setTextColor(Color.WHITE);
        save.setTextColor(Color.WHITE);
        cancel.setTypeface(tf);
        save.setTypeface(tf);
        subject.setTypeface(tf);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_subject, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void save(View v) {
        String s = subject.getText().toString().trim();
        if (s.equals("")) {
            Toast.makeText(this, "Please enter a subject name!", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("subject", s.trim());
        setResult(RESULT_OK, intent);
        finish();
    }

    public void cancel(View v) {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }
}
