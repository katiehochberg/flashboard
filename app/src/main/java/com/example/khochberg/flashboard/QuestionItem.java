package com.example.khochberg.flashboard;

/**
 * Created by Kathryn on 2/27/2015.
 */
public class QuestionItem {

    private String subject;
    private String question;
    private String answer;

    public QuestionItem(String s, String q, String a) {
        subject = s.trim();
        question = q.trim();
        answer = a.trim();
    }

    public String getSubject() {
        return subject;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

}
